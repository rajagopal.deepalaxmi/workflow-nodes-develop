Development
===========

When installing the package from source for development instead, it is recommended to
install it in editable mode, which simply creates a link to the sources so all changes
are reflected in the installed package immediately. The command will also install some
additional development dependencies as defined in ``setup.py``.

.. code-block:: shell

    pip3 install -e .[dev]

Especially for development, running the installation inside a virtual environment is
recommended, see `Virtualenv <https://virtualenv.pypa.io/en/latest/>`__ for more
information.

At this point, it is also recommended to install the `pre-commit
<https://pre-commit.com>`__ hooks by running:

.. code-block:: shell

    pre-commit install

Besides pre-commit, `Pylint <https://www.pylint.org>`__ is used as a static code
analysis tool. It can be used to check the whole project by running:

.. code-block:: shell

    pylint workflow_nodes

For contributions, it is recommended to first create a fork of this project and clone
it. The main repository can be added as an additional remote to the cloned project,
often called ``upstream``. Also checkout the ``development`` branch, which is currently
used as the main development branch.

Afterwards, you may follow the following steps as a rough guideline on how to add a new
tool:

* Create a Python file for the tool in a fitting subdirectory (if one exists), e.g. in
  ``workflow_nodes/system/``, and implement the functionality of the tool. You can use
  other existing tools (and tool groups/directories) as a reference. Also, make sure to
  import the tool in the ``main.py`` file of the group, so it is actually added to the
  respective parent command.
* If there are any new dependencies, add them to ``setup.py`` with appropriate
  version ranges, if necessary.
* Add an entry about any new functionality to ``HISTORY.md``. You may also add
  yourself as a contributor to ``AUTHORS.md``.
* Before creating a merge request on the main repository, make sure the GitLab
  CI runs through successfully in your fork.
