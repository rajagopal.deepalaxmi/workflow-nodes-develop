Configuration
=============

Most of the tools within the package can be used directly after installation. Note that
some tools, especially wrapper tools, might require installing additional, non-Python
packages. However, each tool will check for any missing dependencies, packages or tools
before actually running.

In case you are using tools which interact with a Kadi4Mat instance, make sure the
connection is already configured. See the documentation of the `kadi-apy
<https://kadi-apy.readthedocs.io/en/stable/setup/configuration.html>`__ library for more
information.
