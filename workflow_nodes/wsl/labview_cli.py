# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import subprocess
import sys

from xmlhelpy import Integer
from xmlhelpy import option

from .main import wsl
from workflow_nodes.utils import check_binary


@wsl.command()
@option(
    "operation-name",
    char="o",
    description="Name of LabVIEWCLI operation (eg: MassCompile, CloseLabVIEW, RunVI,"
    " ...)",
)
@option(
    "port-number",
    char="n",
    description="Port on which the remote LabVIEW application is listening",
    param_type=Integer,
)
@option(
    "labview-path",
    char="p",
    description="Path of the LabVIEW in which the operation will run",
)
@option("logfile-path", char="l", description="Path of the LabVIEWCLI log file.")
@option(
    "no-log-to-console",
    char="c",
    description="If set, the output gets logged only to log file (otherwise log file"
    " and console)",
    is_flag=True,
)
@option(
    "verbosity",
    char="v",
    description="This command line argument is used to control the output being logged."
    ' Default is "Default". Possible values are Minimal, Default, Detailed, Diagnostic',
    default="Default",
)
@option(
    "additional-operation-directory",
    char="a",
    description="Additional directory where LabVIEWCLI will look for additional"
    " operation class other than default location",
)
def labview_cli(
    operation_name,
    port_number,
    labview_path,
    logfile_path,
    no_log_to_console,
    verbosity,
    additional_operation_directory,
):
    """Wrapper node for LabVIEWCLI on MS Windows."""
    check_binary("LabVIEWCLI")

    cmd = ["LabVIEWCLI"]

    if operation_name:
        cmd += ["-OperationName", operation_name]
    if port_number:
        cmd += ["-PortNumber", str(port_number)]
    if labview_path:
        cmd += ["-LabVIEWPath", labview_path]
    if logfile_path:
        cmd += ["-LogFilePath", logfile_path]
    if no_log_to_console:
        cmd += ["-LogToConsole", "False"]
    if verbosity:
        cmd += ["-Verbosity", verbosity]
    if additional_operation_directory:
        cmd.append("-AdditionalOperationalDirectory")

    sys.exit(subprocess.run(cmd).returncode)
